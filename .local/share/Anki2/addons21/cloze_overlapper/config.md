## Experimental Cloze Overlapper Settings

This is a collection of experimental settings that still need a bit more testing before moving into the proper settings menu.

Right now you can use these settings to modify Cloze Overlapper's default key bindings. Please note that for changes to the key bindings to take effect, you will have to switch out of the editor/reviewer and back, respectively.

Please also note that other add-ons or system-wide key bindings might interfere with your key assignments, so you might have to experiment a bit until you find a key binding that works without conflicting with anything else.
